const kochibutton = document.getElementById('kochibutton')
const palakkadbutton = document.getElementById('palakkadbutton')
const cityspan = document.getElementById('city')
const temperaturespan = document.getElementById('temperature')

kochibutton.addEventListener('click',()=> {showtemp(9.9312,76.2673 ,"Kochi")})
palakkadbutton.addEventListener('click',()=> {showtemp(10.7867,76.6548 ,"Palakkad")} )

async function showtemp(latitude ,longitude,city){
    const temperature = await gettemperature(latitude,longitude)
     updatescreen(temperature,city)
  }

async function gettemperature(latitude,longitude,city) {
    const response = await fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
    const data =await response.json()
    const temperature = data.current.temperature_2m
    return temperature

}
function updatescreen(temperature,city) {
    temperaturespan.innerHTML = `${temperature}&deg;C`
    cityspan.innerHTML = city  
}